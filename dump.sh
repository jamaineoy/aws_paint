sudo sh -c "echo '
#!/bin/bash
#description: JSPaint web app
#chkconfig: 2345 99 99
case $1 in
	'start')
		cd ~/jspaint
		nohup npm run dev &
		;;
	'stop')
		kill -9 $(ps -ef | egrep 'npm|node' | awk '{print $2}')
		;;
esac' >> /etc/init.d/jspaint"

sudo chmod +x /etc/init.d/jspaint
sudo chkconfig --add jspaint
sudo systemctl enable jspaint
sudo systemctl start jspaint


sudo yum -y install httpd
#sudo touch /etc/httpd/conf/jspaint.conf
sudo sh -c "echo '<VirtualHost *:80>
    ProxyPreserveHost On
    ProxyPass / http://127.0.0.1:8080/
    ProxyPassReverse / http://127.0.0.1:8080/
</VirtualHost>' >> ~/etc/httpd/conf/jspaint.conf"

if rpm -qa | grep "httpd-[0-9]" >/dev/null 2>&1
then
    sudo systemctl start httpd
else
    exit 240
fi

# scp -i ~/.ssh/JamaineObiri-YeboahKey.pem webserver.sh  ec2-user@34.242.7.0:webserver.sh
#sudo sh -c "cat >/etc/init.d/jspaint << _end_
##!/bin/bash
##description: JSPaint web app
##chkconfig: 2345 99 99
#case $1 in
#	"start")
#		cd ~/jspaint
#		nohup npm i
#		nohup npm run dev &
#		;;
#	"stop")
#		kill -9 $(ps -ef | egrep "npm|node" | awk '{print $2}')
#		;;
#esac
#_end_"