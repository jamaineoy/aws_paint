#!/bin/bash

if (( $# > 0 )) # Checks num of args given to stdin (cli args)
then
    # hostname is needed for this
    hostname=$1
else
    echo "WTF! Supply a hostname/IP address" 1>&2
	# Move and merge stdout to stderr
    exit 1
fi

## sudo /etc/init.d/jspaint start
scp -o StrictHostKeyChecking=no -i ~/.ssh/JamaineObiri-YeboahKey.pem jspaint.init ec2-user@$hostname:jspaint.init
## rm -rf *
ssh -o StrictHostKeyChecking=no -i ~/.ssh/JamaineObiri-YeboahKey.pem ec2-user@$hostname '
VERSION=v14.16.1
DISTRO=linux-x64
NODECHECK=no
sudo mkdir -p /usr/local/lib/nodejs 
wget https://nodejs.org/dist/$VERSION/node-$VERSION-$DISTRO.tar.xz 
sudo tar -xJvf node-$VERSION-$DISTRO.tar.xz -C /usr/local/lib/nodejs 

echo "# Nodejs 
VERSION=v14.16.1 
DISTRO=linux-x64
export PATH=/usr/local/lib/nodejs/node-$VERSION-$DISTRO/bin:$PATH" >> ~/.bash_profile 
source ~/.bash_profile


sudo yum -y install git 
git clone https://github.com/1j01/jspaint.git 
cd ~/jspaint
npm i 

sudo yum -y install httpd 
if rpm -qa | grep "httpd-[0-9]" >/dev/null 2>&1
then
    sudo systemctl start httpd 
else
    exit 240
fi

sudo sh -c "echo \"<VirtualHost *:80>
    ProxyPreserveHost On
    ProxyPass / http://127.0.0.1:8080/
    ProxyPassReverse / http://127.0.0.1:8080/
</VirtualHost>\" > /etc/httpd/conf.d/jspaint.conf"
sudo systemctl restart httpd 

sudo mv ~/jspaint.init /etc/init.d/jspaint 
sudo chmod +x /etc/init.d/jspaint
sudo sed -i "s,PATH=\$PATH,PATH=\$PATH:/usr/local/lib/nodejs/node-$VERSION-$DISTRO/bin/," /etc/init.d/jspaint
sudo chkconfig --add jspaint
sudo systemctl enable jspaint
sudo /etc/init.d/jspaint start
'