# Setting up AWS Machine ec2 with JSPaint
This project we will make a bash script to provision a machine in aws to run JSPaint

## Topics covered
This project will help us cover:
1. Git
2. Bitbucket
3. Markdown
4. AWS 
   1. ec2 creation
   2. AWS security groups
5. Redhat
   1. Installation Process
   2. Scripts
6. Bash
   1. ssh
   2. scp
   3. here doc
   4. Outputs (stdin, stdout & stderr)
   5. Piping & Grep

## Explanation of Commands
### Installing JSPaint onto AWS Web
1. Install git
    ```
    $ sudo yum -y install git
    ```
2. Cloned git repo
   ```
    $ git clone https://github.com/1j01/jspaint.git
   ```

3. Run JSPaint
    ```
    ```

---
## Installing JSpaint on to AWS Webserver 
1. Install git
``` 
$ sudo yum -y install git
```
2. Cloned jspaint
```
$ git clone https://github.com/1j01/jspaint.git
$ cd jspaint
```
3. Install nodejs
```
$ VERSION=v14.16.1
$ DISTRO=linux-x64
$ sudo mkdir -p /usr/local/lib/nodejs
$ wget https://nodejs.org/dist/v14.16.1/node-v14.16.1-linux-x64.tar.xz 
$ sudo tar -xJvf node-$VERSION-$DISTRO.tar.xz -C /usr/local/lib/nodejs 
$ npm i ./bash_profile
```
```
$ echo '# Nodejs 
VERSION=v14.16.1 
DISTRO=linux-x64 
export PATH=/usr/local/lib/nodejs/node-$VERSION-$DISTRO/bin:$PATH' >>./bash_profile 
source ~/.bash_profile
(checks if the right version is installed)
node -v 
```
4. Reverse Proxy
```
$ cd /etc/httpd/conf.d
$ touch jspaint.conf
$ echo '<VirtualHost *:80>
    ProxyPreserveHost On
    ProxyPass / http://127.0.0.1:8080/
    ProxyPassReverse / http://127.0.0.1:8080/
</VirtualHost>' >>jspaint.conf
$ sudo systemctl restart httpd
$ cd jspaint/
$ npm i (install dependancies)
$ sudo sh -c "cat > /etc/init.d/jspaint <<_end
#!/bin/bash
#description: JSPaint web app
#chkconfig: 2345 99 99
case $1 in
	'start')
		cd /home/ec2-user/jspaint
		nohup npm run dev &
		;;
	'stop')
		kill -9 $(ps -ef | egrep 'npm|node' | awk '{print $2}')
		;;
esac
_end"
$ sudo chmod +x /etc/init.d/jspaint
$ sudo chkconfig --add jspaint
$ sudo systemctl enable jspaint 
$ sudo systemctl start jspaint
```
run your webserver and check


## Ways to Append to file
```
cat >mywebpage.html <<_END_
<h1>Hello</h1>
<p>Web page text</p>
_END_
```

```
echo '# Nodejs 
<h1>Hello</h1>
<p>Web page text</p>' >> mywebpage.html
```


```
no hang up, run the command
$ nohup

run process in background
$ &
```
