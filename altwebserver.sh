#!/bin/bash
if (( $# > 0 )) 
then
  hostname=$1 ## $ stands for input arguments in command line 
else
  echo "WTF: you must supply a hostname or IP address" 1>&2  ## (1>&2): map stdout to stderr
  exit 1 ## exit 1 (anything that not 0) is recognized a fail exit 
  ## this whole else is a fail message 
fi
## (-o) option allows for the following option StrictHostKeyChecking (-o allows for a lot more options not just StrictHostKeyChecking)
## StrictHostChecking is an option which doesnt add a fingerprint (when set to no) (less strict checking)
## The single quote allows for multiple commands to be run, needs to be right after the log in (on the same line). if its on the line after, itll: log in to the server, press enter, and wait for input 
## general outline of this script 
### get the code
### get the relevant code compiler (npm)
### install dependancies
### start the server (init script)
### Setup nodejs
### Setup reverse proxy
scp -o StrictHostKeyChecking=no -i ~/.ssh/JamaineObiri-YeboahKey.pem jspaint.init ec2-user@$hostname:jspaint.init 
ssh -o StrictHostKeyChecking=no -i ~/.ssh/JamaineObiri-YeboahKey.pem ec2-user@$hostname ' 
sudo yum -y install httpd
sudo yum -y install git
git clone https://github.com/1j01/jspaint.git
cd ~/jspaint
VERSION=v14.16.1
DISTRO=linux-x64
wget https://nodejs.org/dist/$VERSION/node-$VERSION-$DISTRO.tar.xz
sudo mkdir -p /usr/local/lib/nodejs
sudo tar -xJvf node-$VERSION-$DISTRO.tar.xz -C /usr/local/lib/nodejs
echo "# Nodejs 
VERSION=v14.16.1
DISTRO=linux-x64
export PATH=/usr/local/lib/nodejs/node-$VERSION-$DISTRO/bin:$PATH" >>~/.bash_profile
source ~/.bash_profile
npm i
node -v
echo "installation and tests done"
sudo systemctl start httpd
cd /etc/httpd/conf.d
sudo sh -c "echo \"<VirtualHost *:80>
  ProxyPreserveHost On
  ProxyPass / http://127.0.0.1:8080/
  ProxyPassReverse / http://127.0.0.1:8080/
</VirtualHost>\" >jspaint.conf"
sudo systemctl restart httpd
cd ~/jspaint
sudo mv ~/jspaint.init /etc/init.d/jspaint
sudo chmod +x /etc/init.d/jspaint
sudo sed -i "s,PATH=\$PATH,PATH=\$PATH:/usr/local/lib/nodejs/node-$VERSION-$DISTRO/bin/," /etc/init.d/jspaint
sudo chkconfig --add jspaint
sudo systemctl enable jspaint
sudo /etc/init.d/jspaint start
'